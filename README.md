# September Color Picker

## Usage

The September Color Picker is a stand alone UIFramework that creates a basic color picker component.

It can be configured to hold configurations specific for a text segment, background segment, or neither.

### The following properties of the Color Picker are publically available:

The current state of the text color configuration
- @property (nonatomic,strong) UIColor* textColor;

The current state of the background color configuration
- @property (nonatomic,strong) UIColor* backgroundColor;

Weather the color picker has been closed via the exit button
- @property BOOL closed;

### The following functions of the Color Picker are publically available:

Set the color value for the a particular selected segment, NSUInteger 0 will always be available, NSUInteger 1 is available for pickers configured for both text and background configurations.
-(void)setColorValue:(UIColor*)color forSelectedSegment:(NSUInteger)segment;

Close the picker
-(void)close;

Disable the ability to control the alpha value (transparency)
-(void)disableAlpha;

Enable the ability to control the alpha value (transparency)
-(void)enableAlpha;

Set the editing type, with both true values the Color Picker will display a segmented contorl.
-(void)setEditingTypeWithText:(BOOL)text andBackground:(BOOL)background;

Creates a Color Picker and Optionally assigns a delegate for the Color Picker (See below for Delegate specification)
+(ColorPicker*)colorPickerWithDelegate:(id<ColorPickerDelegate>)delegate;

Access the color picker delegate (See below for Delegate specification)
- @property (nonatomic,weak) id<ColorPickerDelegate> delegate;

@end

The ColorPickerDelegate allows for customization and responses for changing in the color pickers state.
```
@protocol ColorPickerDelegate <NSObject>

// This function is called with the color picker generates a new color, selection is dependent on the inital configuration of the Color Picker
-(void)ColorPicker:(ColorPicker*)picker createdNewColor:(UIColor*)color withSegmentControlValue:(NSInteger)selection;

// This function is called when the exit button of the color picker is pressed.
-(void)ColorPickerPressedExitButton:(ColorPicker*)picker;

// Configuration option for the color picker to set if the picker will have a text specific state
-(BOOL)ColorPickerShouldHaveTextOption:(ColorPicker*)picker;

// Configuration option for the color picker to set if the picker will have a background specific state
-(BOOL)ColorPickerShouldHaveBackgroundOption:(ColorPicker *)picker;

// Configuration option for the color picker to set if the picker will allow for alpha control
-(BOOL)ColorPickerShouldHaveAlphaControl:(ColorPicker *)picker;

@optional

// Configuration option for the color picker to set the text specific color
-(UIColor*)ColorPickerInitalColorForText:(ColorPicker*)picker;

// Configuration option for the color picker to set the background specific color
-(UIColor*)ColorPickerInitalColorForBackground:(ColorPicker*)picker;

@end
```

## Building the Framework

The Framework can be built using XCode
1.) Select the Framework Target from the run configuration
2.) Then select Generic iOS Device as the platform.
3.) The Framework and Bundle Resources will be copied to the machine Desktop
4.) Drag the Framework and Bundle into your project
5.) Enjoy Coloring!!

## Developing on the Framework

Along with the SeptemberColorPicker, the SeptemberColorPickerApp has been brought up to supply a development environment so one can quickly iterate on the Framework. 

In the SeptemberColorPickerApp directory there opening SeptemberColorPickerApp.xcodeproj file will enable you to start up this dev environment.

This Framework was build using the tutorial here: https://www.raywenderlich.com/65964/create-a-framework-for-ios