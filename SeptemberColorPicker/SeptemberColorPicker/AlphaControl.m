//
//  AlphaControl.m
//  Board Share
//
//  Created by Rohan Thomare on 1/22/14.
//  Copyright (c) 2014 TommyRayStudios. All rights reserved.
//



#import "AlphaControl.h"
#import "AlphaControlConstants.h"

float DEFAULT_START = 1.0f;

@interface AlphaControl()

@property (nonatomic, strong) UILabel* alphaLabel;
@property (nonatomic, strong) UISlider* alphaSlider;
@property (strong, nonatomic) NSNumberFormatter *fmt;

@end

@implementation AlphaControl

+ (AlphaControl*) componentWithOrigin:(CGPoint)origin {
    return [[AlphaControl alloc] initWithFrame:CGRectMake(origin.x, origin.y, AlphaControlWidth, AlphaControlHeight)];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // set up number formatter
        _fmt = [[NSNumberFormatter alloc] init];
        [_fmt setNumberStyle:NSNumberFormatterPercentStyle];
        
        // set up label
        _alphaLabel = [[UILabel alloc] initWithFrame:CGRectMake(AlphaControlLabelOffsetX, AlphaControlLabelOffsetY, AlphaControlLabelWidth, AlphaControlLabelHeight)];
        [_alphaLabel setFont:[self getLabelFont]];
        [_alphaLabel setTextColor:[self getLabelColor]];
        [_alphaLabel setTextAlignment:NSTextAlignmentCenter];
        NSNumber* numberValue = [[NSNumber alloc] initWithFloat:DEFAULT_START];
        [self.alphaLabel setText:[_fmt stringFromNumber:numberValue]];
        
        // set up slider
        _alphaSlider = [[UISlider alloc] initWithFrame:CGRectMake(AlphaControlSliderOffsetX, AlphaControlSliderOffsetY, AlphaControlSliderWidth, AlphaControlSliderHeight)];
        [_alphaSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
        [_alphaSlider setValue:DEFAULT_START];
    }
    return self;
}

- (CGFloat)getAlpha
{
    return [self.alphaSlider value];
}

- (UIColor*)getLabelColor {
    return [UIColor colorWithRed:0.0f/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
}

- (UIFont*)getLabelFont {
    return [UIFont fontWithName:@"System Bold" size:12.0f];
}

- (void)setAlphaControlValue:(CGFloat)value {
    [self.alphaSlider setValue:value];
    [self valueChanged:self.alphaSlider];
}

-(void)valueChanged:(UISlider*)sender {
    [self.delegate newAlphaValue:sender.value];
    NSNumber* numberValue = [[NSNumber alloc] initWithFloat:sender.value];
    [self.alphaLabel setText:[self.fmt stringFromNumber:numberValue]];
}

-(void) layoutSubviews {
    [super layoutSubviews];
    [self addSubview:_alphaLabel];
    [self addSubview:_alphaSlider];
}

@end
