//
//  AlphaControlConstants.h
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/16/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef AlphaControlConstants

#pragma mark - Alpha Control Size Constants
extern const CGFloat AlphaControlWidth;
extern const CGFloat AlphaControlHeight;

#pragma mark - Alpha Control Slider Constants
extern const CGFloat AlphaControlSliderWidth;
extern const CGFloat AlphaControlSliderHeight;
extern const CGFloat AlphaControlSliderOffsetX;
extern const CGFloat AlphaControlSliderOffsetY;

#pragma mark - Alpha Control Label Constants
extern const CGFloat AlphaControlLabelWidth;
extern const CGFloat AlphaControlLabelHeight;
extern const CGFloat AlphaControlLabelOffsetX;
extern const CGFloat AlphaControlLabelOffsetY;

#endif
