//
//  AlphaControlConstants.m
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/16/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import "AlphaControlConstants.h"

#pragma mark - Alpha Control Size Constants
const CGFloat AlphaControlWidth = 185.0f;
const CGFloat AlphaControlHeight = 50.0f;

#pragma mark - Alpha Control Slider Constants
const CGFloat AlphaControlSliderWidth = 140.0f;
const CGFloat AlphaControlSliderHeight = 30.0f;
const CGFloat AlphaControlSliderOffsetX = 0.0f;
const CGFloat AlphaControlSliderOffsetY = 18.0f;

#pragma mark - Alpha Control Label Constants
const CGFloat AlphaControlLabelWidth = 45.0f;
const CGFloat AlphaControlLabelHeight = 17.0f;
const CGFloat AlphaControlLabelOffsetX = 142.0f;
const CGFloat AlphaControlLabelOffsetY = 24.0f;
