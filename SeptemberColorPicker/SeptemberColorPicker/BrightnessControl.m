//
//  BrightnessControl.m
//  Board Share
//
//  Created by Rohan Thomare on 1/22/14.
//  Copyright (c) 2014 TommyRayStudios. All rights reserved.
//

#import "BrightnessControl.h"
#import "BrightnessDisplay.h"
#import "BrightnessControlConstants.h"

@interface BrightnessControl()

// we need a better selector
@property (strong, nonatomic) UILabel *selector;
@property (strong, nonatomic) BrightnessDisplay* display;
@property CGFloat maxY;
@property CGFloat minY;

@end

@implementation BrightnessControl

@synthesize minY = _minY;
@synthesize maxY = _maxY;

+(BrightnessControl*)componentWithOrigin:(CGPoint)origin andColor:(UIColor *)color {
    return [[BrightnessControl alloc] initWithFrame:CGRectMake(origin.x, origin.y, BrightnessControlWidth, BrightnessControlHeight) andColor:color];
}

- (id)initWithFrame:(CGRect)frame andColor:(UIColor*)color {
    self = [super initWithFrame:frame];
    if (self) {
        CGRect displayRect = CGRectMake(BrightnessDisplayOriginX, BrightnessDisplayOriginY, BrightnessDisplayWidth, BrightnessDisplayHeight);
        _display = [[BrightnessDisplay alloc] initWithFrame:displayRect andColor:color];
        [self addSubview:_display];
        
        //move slider to top
        _minY = BrightnessDisplayOriginY;
        _maxY = BrightnessDisplayOriginY + BrightnessDisplayHeight - BrightnessDisplayBorderWidth;
        
        // we need a better selector
        _selector = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, BrightnessSelectorWidth, BrightnessSelectorHeight)];
        [_selector setTextAlignment:NSTextAlignmentCenter];
        [_selector setText:@">--"];
        [_selector setFont:[self getLabelFont]];
        [_selector setTextColor:[self getLabelColor]];
        [_selector setCenter:CGPointMake(BrightnessDisplayOriginX+(BrightnessDisplayWidth/2.0f)-5.0f, BrightnessDisplayOriginY)];
        [self addSubview:_selector];
        
        [self addGestures];
    }
    return self;
}

-(void)setColor:(UIColor *)color {
    CGFloat hue, sat, bright, alpha;
    [color getHue:&hue saturation:&sat brightness:&bright alpha:&alpha];
    // want background color to be solid color
    [self.display setColor:color];
    [self setBrightnessValue:bright];
}

-(CGFloat)brightnessValue {
    CGFloat scale = (1.0f/(self.maxY-self.minY));
    CGFloat distance = self.selector.center.y - self.minY;
    return 1.0f - scale*distance;
}

- (void)setBrightnessValue:(CGFloat)value {
    CGFloat scale = (1.0f/(self.maxY-self.minY));
    CGFloat distance = (1.0f - value)/scale;
    CGPoint selectorCenter = self.selector.center;
    selectorCenter.y = self.minY+distance;
    selectorCenter.y = MAX(selectorCenter.y, self.minY);
    selectorCenter.y = MIN(selectorCenter.y, self.maxY - 1.0f);
    [self.selector setCenter:selectorCenter];
}

- (void)move:(UIPanGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateChanged ||
        recognizer.state == UIGestureRecognizerStateEnded) {
        
        CGPoint translation = [recognizer translationInView:self];
        
        CGRect newButtonFrame = self.selector.frame;
        newButtonFrame.origin.y += translation.y;

        CGPoint newButtonCenter = CGPointMake(newButtonFrame.origin.x + (newButtonFrame.size.width/2), newButtonFrame.origin.y + (newButtonFrame.size.height/2));
        newButtonCenter.y = MAX(newButtonCenter.y, self.minY);
        newButtonCenter.y = MIN(newButtonCenter.y, self.maxY - 1.0f);
        
        self.selector.center = newButtonCenter;
        
        [recognizer setTranslation:CGPointZero inView:self];
        [self.delegate newBrightnessValue:[self brightnessValue]];
    }
}

- (void)tap:(UITapGestureRecognizer*)recognizer {
    if(recognizer.state == UIGestureRecognizerStateChanged || recognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint tap = [recognizer locationOfTouch:0 inView:self];
        
        if(tap.y >= self.maxY || tap.y < self.minY)
            return;
        
        CGPoint newButtonCenter = self.selector.center;
        newButtonCenter.y = tap.y;
        
        [self.selector setCenter:newButtonCenter];
        [self.delegate newBrightnessValue:[self brightnessValue]];
    }
}

-(void)addGestures {
    //add gesture to tap on color wheel to obtain new hue and saturation
    UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
    [tapgr setNumberOfTapsRequired:1];
    [self addGestureRecognizer:tapgr];
    
    UIPanGestureRecognizer *pangr = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [self addGestureRecognizer:pangr];
}

- (UIColor*)getLabelColor {
    return [UIColor colorWithRed:0.0f/255.0f green:122.0f/255.0f blue:255.0f/255.0f alpha:1.0f];
}

- (UIFont*)getLabelFont {
    return [UIFont fontWithName:@"System Bold" size:15.0f];
}

@end
