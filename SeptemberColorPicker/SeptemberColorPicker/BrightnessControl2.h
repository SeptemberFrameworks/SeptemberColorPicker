//
//  BrightnessDisplay2.h
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/11/17.
//  Copyright © 2017 Rohan Thomare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface BrightnessControl2 : UIView

- (id)initWithFrame:(CGRect)frame andColor:(UIColor*)color;

@end
