//
//  BrightnessDisplay2.m
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/11/17.
//  Copyright © 2017 Rohan Thomare. All rights reserved.
//

#import "BrightnessControl2.h"
#import "BrightnessDisplay.h"
#import "BrightnessControlConstants.h"

@interface BrightnessControl2()

@property (strong, nonatomic) UIView *selector;
@property (strong, nonatomic) BrightnessDisplay* display;
@property CGFloat maxY;
@property CGFloat minY;
@property CGFloat hue;
@property CGFloat saturation;
@property CGFloat brightness;
//@property CGFloat alpha;

@end

@implementation BrightnessControl2

- (id)initWithFrame:(CGRect)frame andColor:(UIColor*)color {
    self = [super initWithFrame:frame];
    if (self) {
        CGRect displayRect = CGRectMake(BrightnessDisplayOriginX, BrightnessDisplayOriginY, BrightnessDisplayWidth, BrightnessDisplayHeight);
        _display = [[BrightnessDisplay alloc] initWithFrame:displayRect andColor:color];
        [self addSubview:_display];
        
        //move slider to top
        _minY = BrightnessDisplayOriginY;
        _maxY = BrightnessDisplayOriginY + BrightnessDisplayHeight;
        _selector = [[UIView alloc] initWithFrame:CGRectMake(0, 0, BrightnessControlWidth, BrightnessSelectorHeight)];
//        [_selector setTextAlignment:NSTextAlignmentCenter];
//        [_selector setText:@">--"];
        [self addSubview:_selector];
        
        [self setBackgroundColor:[UIColor whiteColor]];
        [self addGestures];
    }
    return self;
}


- (void)t:(UITapGestureRecognizer*)r {
    NSLog(@"Tap");
}

-(void)move:(UIPanGestureRecognizer *)recognizer {
    NSLog(@"Move");
//    if (recognizer.state == UIGestureRecognizerStateChanged ||
//        recognizer.state == UIGestureRecognizerStateEnded) {
//        
//        UIView *draggedButton = recognizer.view;
//        CGPoint translation = [recognizer translationInView:self];
//        
//        CGRect newButtonFrame = draggedButton.frame;
//        newButtonFrame.origin.y += translation.y;
//        
//        CGPoint newButtonCenter = CGPointMake(newButtonFrame.origin.x + (newButtonFrame.size.width/2), newButtonFrame.origin.y + (newButtonFrame.size.height/2));
//        if(newButtonCenter.y <= self.minY)
//            newButtonCenter.y = self.minY;
//        else if(newButtonCenter.y >=  self.maxY)
//            newButtonCenter.y = self.maxY;
//        
//        draggedButton.center = newButtonCenter;
//        
//        [recognizer setTranslation:CGPointZero inView:self];
//        [self.delegate newBrightnessValue:[self brightnessValue]];
//    }
}

- (void)addGestures {
        // add gesture to tap on color wheel to obtain new hue and saturation
//        UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(t:)];
//        [tapgr setNumberOfTapsRequired:1];
//        [self addGestureRecognizer:tapgr];
    
        UIPanGestureRecognizer *pangr = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
        [self.selector addGestureRecognizer:pangr];
}

@end
