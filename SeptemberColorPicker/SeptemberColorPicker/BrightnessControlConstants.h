//
//  BrightnessControlConstants.h
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/16/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef BrightnessControlConstants

#pragma mark - Brightness Size Constants
extern const CGFloat BrightnessControlWidth;
extern const CGFloat BrightnessControlHeight;

#pragma mark - Brightness Display Constants
extern const CGFloat BrightnessDisplayWidth;
extern const CGFloat BrightnessDisplayHeight;
extern const CGFloat BrightnessDisplayOriginX;
extern const CGFloat BrightnessDisplayOriginY;
extern const int BrightnessDisplayBorderWidth;

#pragma mark - Brightness Display Constants
extern const CGFloat BrightnessSelectorWidth;
extern const CGFloat BrightnessSelectorHeight;

#endif
