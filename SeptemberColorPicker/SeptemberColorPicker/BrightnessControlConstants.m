//
//  BrightnessControlConstants.m
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/16/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef BrightnessControlConstants

#pragma mark - Brightness Size Constants
const CGFloat BrightnessControlWidth = 29.0f;
const CGFloat BrightnessControlHeight = 197.0f;

#pragma mark - Brightness Display Constants
const CGFloat BrightnessDisplayWidth = 18.0f;
const CGFloat BrightnessDisplayHeight = 173.0f;
const CGFloat BrightnessDisplayOriginX = 11.0f;
const CGFloat BrightnessDisplayOriginY = 8.0f;
const int BrightnessDisplayBorderWidth = 1.0f;

#pragma mark - Brightness Display Constants
const CGFloat BrightnessSelectorWidth = 35.0f;
const CGFloat BrightnessSelectorHeight = 30.0f;

#endif
