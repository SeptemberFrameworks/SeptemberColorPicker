//
//  BrightnessDisplay.h
//  Board Share
//
//  Created by Rohan Thomare on 1/22/14.
//  Copyright (c) 2014 TommyRayStudios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrightnessDisplay : UIView

- (id)initWithFrame:(CGRect)frame andColor:(UIColor*)color;

- (void)setColor:(UIColor*)color;

@end
