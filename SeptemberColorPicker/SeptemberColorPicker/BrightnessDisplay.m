
//
//  BrightnessDisplay.m
//  Board Share
//
//  Created by Rohan Thomare on 1/22/14.
//  Copyright (c) 2014 TommyRayStudios. All rights reserved.
//

#import "BrightnessDisplay.h"
#import "BrightnessControlConstants.h"

@implementation BrightnessDisplay

- (id)initWithFrame:(CGRect)frame andColor:color {
    self = [super initWithFrame:frame];
    if (self) {
        [self setColor:color];
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = BrightnessDisplayBorderWidth;
    }
    return self;
}

- (void)setColor:(UIColor*)color {
    CGFloat hue, saturation, brightness, alpha;
    [color getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
    self.backgroundColor = [UIColor colorWithHue:hue saturation:saturation brightness:1.0f alpha:1.0f];
}

// Draw Shading Down For Birghtness Display
- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsPushContext(context);
    [self drawForRect:rect andContext:context];
    UIGraphicsPopContext();
}

// Draws Shading Down For Particular Color
-(void)drawForRect:(CGRect)rect andContext:(CGContextRef)context {
    CGFloat width = rect.size.width;
    CGFloat height = rect.size.height;
    CGFloat scale = (1.0/height);
    //draw pixels
    for (int y = 0; y < height; y++) {
        CGFloat newAlpha = (CGFloat)(y)*scale;
        const CGFloat value[4] = {0.0f, 0.0f, 0.0f, newAlpha};
        for (int x = 0; x < width; x++) {
            CGContextSetFillColor(context, value);
            CGContextFillRect(context, CGRectMake(x, y, 1.0f, 1.0f));
        }
    }
}

@end
