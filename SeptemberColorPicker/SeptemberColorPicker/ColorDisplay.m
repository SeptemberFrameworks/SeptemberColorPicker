//
//  ColorDisplay.m
//  Board Share
//
//  Created by Rohan Thomare on 1/22/14.
//  Copyright (c) 2014 TommyRayStudios. All rights reserved.
//

#import "ColorDisplay.h"
#import "BrightnessControlConstants.h"

@interface ColorDisplay()

@property (nonatomic, strong) UIImageView* backgroundChecker;
@property (nonatomic, strong) UIView* color;

@end

@implementation ColorDisplay

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Allocate subviews
        CGRect subViewFrame = CGRectMake(0.0, 0.0, frame.size.width, frame.size.height);
        _backgroundChecker = [[UIImageView alloc] initWithFrame:subViewFrame];
        _backgroundChecker.image = [UIImage imageNamed:@"SeptemberColorPicker.bundle/checkeredBackground"];
        [self addSubview:_backgroundChecker];
        
        _color = [[UIView alloc] initWithFrame:subViewFrame];
        [_color setBackgroundColor:[UIColor whiteColor]];
        _color.layer.borderColor = [UIColor blackColor].CGColor;
        _color.layer.borderWidth = BrightnessDisplayBorderWidth;
        [self addSubview:_color];
    }
    return self;
}

- (void)updateDisplayWithColor:(UIColor*)color
{
    // Called to set the color of the display
    [_color setBackgroundColor:color];
    // Adversley effects performance if spammed
    [_color setNeedsDisplay];
}

@end
