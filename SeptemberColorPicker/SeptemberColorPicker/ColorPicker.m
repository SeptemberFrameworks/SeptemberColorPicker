//
//  ColorPicker.m
//  Board Share
//
//  Created by Rohan Thomare on 1/22/14.
//  Copyright (c) 2014 TommyRayStudios. All rights reserved.
//

#import "ColorPicker.h"
#import "ColorPickerUIConstants.h"
#import "ColorDisplay.h"
#import "BrightnessControl.h"
#import "AlphaControl.h"
#import "ColorWheel.h"
#import "BrightnessControl.h"
#import "ColorPickerDelegate.h"

NSString* BACKGROUND_TITLE = @"Background";
NSString* TEXT_TITLE = @"Text";

@interface ColorPicker() <AlphaReciverDelegate,BrightnessRecieverDelegate,ColorRecieverDelegate,UITextFieldDelegate>

@property (strong, nonatomic) UISegmentedControl *textBackgroundPicker;

@property (strong, nonatomic) ColorDisplay *colorDisplay;

@property (strong, nonatomic) BrightnessControl *brightnessControl;

@property (strong, nonatomic) AlphaControl *alphaSlider;

@property (strong, nonatomic) ColorWheel *wheel;

@property (strong, nonatomic) UIButton *closeButton;

@property (strong, nonatomic) UITextField *hueText;

@property (strong, nonatomic) UITextField *satText;

@property (strong, nonatomic) UITextField *brightnessText;

@property BOOL textEdit;

@property BOOL backgroundEdit;

@property CGFloat textHueValue;
@property CGFloat textSaturationValue;
@property CGFloat textBrightnessValue;
@property CGFloat textAlphaValue;
@property CGPoint textSelectorPoint;

@property CGFloat backgroundHueValue;
@property CGFloat backgroundSaturationValue;
@property CGFloat backgroundBrightnessValue;
@property CGFloat backgroundAlphaValue;
@property CGPoint backgroundSelectorPoint;

@end

@implementation ColorPicker

@synthesize delegate = _delegate;

+(ColorPicker*)colorPickerWithDelegate:(id<ColorPickerDelegate>)delegate {
    ColorPicker* colorPicker = [[ColorPicker alloc] initWithFrame:CGRectMake(0, 0, CPHorizontalSize, CPVerticalSize)];
    [colorPicker setDelegate:delegate];
    return colorPicker;
}

-(void)setDelegate:(id<ColorPickerDelegate>)delegate {
    _delegate = delegate;
    if(_delegate) {
        [self setEditingTypeWithText:[_delegate ColorPickerShouldHaveTextOption:self] andBackground:[_delegate ColorPickerShouldHaveBackgroundOption:self]];
        
        if(self.textEdit && self.backgroundEdit) {
            if([_delegate respondsToSelector:@selector(ColorPickerInitalColorForText:)])
                [self setColorValue:[_delegate ColorPickerInitalColorForText:self] forSelectedSegment:0];
            
            if([_delegate respondsToSelector:@selector(ColorPickerInitalColorForBackground:)])
                [self setColorValue:[_delegate ColorPickerInitalColorForBackground:self] forSelectedSegment:1];
        } else if(self.textEdit) {
            if([_delegate respondsToSelector:@selector(ColorPickerInitalColorForText:)])
                [self setColorValue:[_delegate ColorPickerInitalColorForText:self] forSelectedSegment:0];
        }
        else if(self.backgroundEdit) {
            if([_delegate respondsToSelector:@selector(ColorPickerInitalColorForBackground:)])
                [self setColorValue:[_delegate ColorPickerInitalColorForBackground:self] forSelectedSegment:0];
        }
        
        if([_delegate ColorPickerShouldHaveAlphaControl:self])
            [self enableAlpha];
        else
            [self disableAlpha];
    }
}

- (void)setInitalValues {
    // set default to black
    self.textBrightnessValue = 1.0f;
    self.textHueValue = 1.0f;
    self.textSaturationValue = 1.0f;
    self.textAlphaValue = 1.0f;
    
    // set default to white
    self.backgroundBrightnessValue = 1.0f;
    self.backgroundHueValue = 0.0f;
    self.backgroundSaturationValue = 0.0f;
    self.backgroundAlphaValue = 1.0f;
    
    self.backgroundEdit = YES;
    self.textEdit = YES;
    self.closed = NO;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        //set inital values
        [self setInitalValues];
    }
    [self setupView];
    return self;
}

-(void)setColorValue:(UIColor*)color forSelectedSegment:(NSUInteger)segment {
    CGColorSpaceRef colorSpace = CGColorGetColorSpace([color CGColor]);
    CGColorSpaceModel colorSpaceModel = CGColorSpaceGetModel(colorSpace);
    
    if(colorSpaceModel == kCGColorSpaceModelMonochrome) {
        CGFloat _white = 0.0f, _alpha = 0.0f;
        [color getWhite:&(_white) alpha:&(_alpha)];
        color = [[UIColor alloc] initWithRed:_white green:_white blue:_white alpha:_alpha];
    }

    if(segment == 0) {
        [color getHue:&(_textHueValue) saturation:&(_textSaturationValue) brightness:&(_textBrightnessValue) alpha:&(_textAlphaValue)];
        if(segment == self.textBackgroundPicker.selectedSegmentIndex) {
            [self.brightnessControl setBrightnessValue:_textBrightnessValue];
            [self.colorDisplay updateDisplayWithColor:color];
            [self.alphaSlider setAlphaControlValue:_textAlphaValue];
        }
    } else {
        [color getHue:&(_backgroundHueValue) saturation:&(_backgroundSaturationValue) brightness:&(_backgroundBrightnessValue) alpha:&(_backgroundAlphaValue)];
        if(segment == self.textBackgroundPicker.selectedSegmentIndex) {
            [self.brightnessControl setBrightnessValue:_backgroundBrightnessValue];
            [self.colorDisplay updateDisplayWithColor:color];
            [self.alphaSlider setAlphaControlValue:_backgroundAlphaValue];
        }
    }
    [self redrawView];
}

-(void)redrawView {
    UIColor* color = [self chosenColor];
    CGFloat hue, sat, bright, alpha;
    [self getCurrentHue:&hue saturation:&sat brightness:&bright andAlphaValues:&alpha];
    [self.brightnessControl setColor:color];
    [self.colorDisplay updateDisplayWithColor:color];
    [self.hueText setText:[@((int)(hue*360)) stringValue]];
    [self.brightnessText setText:[@((int)(sat*100)) stringValue]];
    [self.satText setText:[@((int)(bright*100)) stringValue]];
    [self.delegate ColorPicker:self createdNewColor:color withSegmentControlValue:self.textBackgroundPicker.selectedSegmentIndex];
}

-(void)setHue:(CGFloat)hue {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0) {
        self.textHueValue = hue;
    } else {
        self.backgroundHueValue = hue;
    }
}

-(void)setSaturation:(CGFloat)saturation {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0) {
        self.textSaturationValue = saturation;
    } else {
        self.backgroundSaturationValue = saturation;
    }
}

-(void)setBrightness:(CGFloat)brightness {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0) {
        self.textBrightnessValue = brightness;
    } else {
        self.backgroundBrightnessValue = brightness;
    }
}

-(void)setSelectorPoint:(CGPoint)selectorPoint {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0) {
        self.textSelectorPoint = selectorPoint;
    } else {
        self.backgroundSelectorPoint = selectorPoint;
    }
}

-(CGFloat)getHue {
    return self.textHueValue ? self.textBackgroundPicker.selectedSegmentIndex == 0 : self.backgroundHueValue;
}

-(CGFloat)getSaturation {
    return self.textSaturationValue ? self.textBackgroundPicker.selectedSegmentIndex == 0 : self.backgroundSaturationValue;
}

-(CGFloat)getBrightness {
    return self.textBrightnessValue ? self.textBackgroundPicker.selectedSegmentIndex == 0 : self.backgroundBrightnessValue;
}

//-(CGPoint)getSelectorPoint {
//    return self.textSelectorPoint ? self.textBackgroundPicker.selectedSegmentIndex == 0 : self.backgroundSelectorPoint;
//}

-(void)getCurrentHue:(CGFloat*)hue saturation:(CGFloat*)sat brightness:(CGFloat*)brightness andAlphaValues:(CGFloat*)
alpha {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0) {
        *hue = self.textHueValue;
        *sat = self.textSaturationValue;
        *brightness = self.textBrightnessValue;
        *alpha = self.textAlphaValue;
    } else {
        *hue = self.backgroundHueValue;
        *sat = self.backgroundSaturationValue;
        *brightness = self.backgroundBrightnessValue;
        *alpha = self.backgroundAlphaValue;
    }
}

-(UIColor*)chosenColor {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0)
        return [UIColor colorWithHue:self.textHueValue saturation:self.textSaturationValue brightness:self.textBrightnessValue alpha:self.textAlphaValue];
    else
        return [UIColor colorWithHue:self.backgroundHueValue saturation:self.backgroundSaturationValue brightness:self.backgroundBrightnessValue alpha:self.backgroundAlphaValue];
}

-(void)newAlphaValue:(CGFloat)alpha {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0)
        self.textAlphaValue = alpha;
    else
        self.backgroundAlphaValue = alpha;
    [self redrawView];
}

-(void)newBrightnessValue:(CGFloat)brightness {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0)
        self.textBrightnessValue = brightness;
    else
        self.backgroundBrightnessValue = brightness;
    [self redrawView];

}

-(void)newHueValue:(CGFloat)hue andSaturationValue:(CGFloat)saturation atPoint:(CGPoint)point {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0) {
        self.textHueValue = hue;
        self.textSaturationValue = saturation;
        self.textSelectorPoint = point;
    } else {
        self.backgroundHueValue = hue;
        self.backgroundSaturationValue = saturation;
        self.backgroundSelectorPoint = point;
    }
    [self redrawView];
}

- (void)selectorValueChanged:(UISegmentedControl *)sender
{
    if(sender.selectedSegmentIndex == 0) {
//        [self.wheel setColorAtPointForHue:self.textHueValue andSaturation:self.textSaturationValue];
        [self.wheel setSelectorAtPoint:self.textSelectorPoint];
        [self.alphaSlider setAlphaControlValue:self.textAlphaValue];
        [self.brightnessControl setBrightnessValue:self.textBrightnessValue];
    } else {
//        [self.wheel setColorAtPointForHue:self.backgroundHueValue andSaturation:self.backgroundSaturationValue];
        [self.wheel setSelectorAtPoint:self.backgroundSelectorPoint];
        [self.alphaSlider setAlphaControlValue:self.backgroundAlphaValue];
        [self.brightnessControl setBrightnessValue:self.backgroundBrightnessValue];
    }
    [self redrawView];
}

-(void)setupCloseButton {
    CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI_4);
    self.closeButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
    [self.closeButton setCenter:CGPointMake(CPCloseButtonXCenter, CPCloseButtonYCenter)];
    self.closeButton.transform = transform;
    [self.closeButton addTarget:self action:@selector(closeButtonPressed:) forControlEvents: UIControlEventTouchUpInside];
    [self addSubview:self.closeButton];
}

-(void)setupSegmentedView {
    NSMutableArray* items = [[NSMutableArray alloc] init];
    if(self.textEdit)
        [items addObject:TEXT_TITLE];
    if(self.backgroundEdit)
        [items addObject:BACKGROUND_TITLE];
    self.textBackgroundPicker = [[UISegmentedControl alloc] initWithItems:items];
    self.textBackgroundPicker.frame = CGRectMake(CPSegmentXOrigin, CPSegmentYOrigin, CPSegmentWidth, CPSegmentHeight);
    [self.textBackgroundPicker addTarget:self action:@selector(selectorValueChanged:) forControlEvents: UIControlEventValueChanged];
    self.textBackgroundPicker.selectedSegmentIndex = 0;
    [self addSubview:self.textBackgroundPicker];
}

-(void)setupAlphaControl {
    _alphaSlider = [AlphaControl componentWithOrigin:CGPointMake(CPAlphaControlXOrigin, CPAlphaControlYOrigin)];
    _alphaSlider.delegate = self;
    [self addSubview:_alphaSlider];
}

-(void)setupBrightnessControl {
    _brightnessControl = [BrightnessControl componentWithOrigin:CGPointMake(CPBrightnessControlXOrigin, CPBrightnessControlYOrigin) andColor:[self chosenColor]];
    [_brightnessControl setDelegate:self];
    [self addSubview:_brightnessControl];
}

-(void)setupColorDisplay {
    _colorDisplay = [[ColorDisplay alloc] initWithFrame:CGRectMake(CPDisplayXOrigin, CPDisplayYOrigin, CPDisplayWidth, CPDisplayHeight)];
    [_colorDisplay updateDisplayWithColor:[self chosenColor]];
    [self addSubview:_colorDisplay];
}

-(void)setupColorWheel {
    _wheel = [ColorWheel wheelWithOrigin:CGPointMake(CPWheelXOrigin,CPWheelYOrigin) andDelegate:self];
    [self addSubview:_wheel];
}


-(UITextField*)textFieldOriginX:(CGFloat)x {
    CGRect rect = CGRectMake(x, TextFieldOriginY, TextFieldWidth, TextFieldHeight);
    UITextField* textField = [[UITextField alloc] initWithFrame:rect];
    [textField setKeyboardType:UIKeyboardTypeNumberPad];
    [textField setBackgroundColor:[UIColor grayColor]];
    [textField setTextColor:[UIColor whiteColor]];
    [textField setTextAlignment:NSTextAlignmentCenter];
    [textField setDelegate:self];
    return textField;
}

-(void)textEditors {
    CGFloat xIter = TextFieldOriginX;
    _hueText = [self textFieldOriginX:xIter];
    xIter += TextFieldSpacing + TextFieldWidth;
    _brightnessText = [self textFieldOriginX:xIter];
    xIter += TextFieldSpacing + TextFieldWidth;
    _satText = [self textFieldOriginX:xIter];
    
    [self addSubview:_hueText];
    [self addSubview:_brightnessText];
    [self addSubview:_satText];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self textFieldDidEndEditing:textField];
    return YES;
}


-(void)textFieldDidEndEditing:(UITextField *)textField {
    CGFloat value = [textField.text floatValue];
    if (textField == _hueText) {
        if(self.textBackgroundPicker.selectedSegmentIndex == 0) {
            self.textHueValue = value;
        } else {
            self.backgroundHueValue = value;
        }
    }
    if (textField == _brightnessText) {
        if(self.textBackgroundPicker.selectedSegmentIndex == 0) {
            self.textBrightnessValue = value;
        } else {
            self.backgroundBrightnessValue = value;
        }
    }
    if (textField == _satText) {
        if(self.textBackgroundPicker.selectedSegmentIndex == 0) {
            self.textSaturationValue = value;
        } else {
            self.backgroundSaturationValue = value;
        }
    }
//    [self setColorValue:color forSelectedSegment:self.textBackgroundPicker.numberOfSegments];
}

-(void)setupView {
    // Add Background Of View
    UIToolbar* background = [[UIToolbar alloc] initWithFrame:self.frame];
    [background setBarStyle:UIBarStyleBlack];
    [background setTranslucent:YES];
    [self addSubview:background];
    
    // Add Close Button
    [self setupCloseButton];
    
    // Add Segmented View
    [self setupSegmentedView];
    
    // Add AlphaControl
    [self setupAlphaControl];
    
    // Add BrightnessControl
    [self setupBrightnessControl];
    
    // Add ColorDisplay
    [self setupColorDisplay];
    
    // Add ColorWheel
    [self setupColorWheel];
    
    // Add textEditors
    [self textEditors];
    
}

- (void)closeButtonPressed:(id)sender {
    [self.delegate ColorPickerPressedExitButton:self];
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self redrawView];
}

-(void)disableAlpha {
    [self.alphaSlider setAlphaControlValue:1.0f];
    [self.alphaSlider setAlpha:0.0];
}

-(void)enableAlpha {
    if(self.textBackgroundPicker.selectedSegmentIndex == 0)
        [self.alphaSlider setAlpha:self.textAlphaValue];
    else
        [self.alphaSlider setAlpha:self.backgroundAlphaValue];
    [self.alphaSlider setAlpha:1.0];
}

-(void)setEditingTypeWithText:(BOOL)text andBackground:(BOOL)background {
    self.textEdit = text;
    self.backgroundEdit = background;
    if(text && background) {
        if(self.textBackgroundPicker.numberOfSegments == 1) {
            [self.textBackgroundPicker setTitle:@"Text" forSegmentAtIndex:0];
            [self.textBackgroundPicker insertSegmentWithTitle:@"Background" atIndex:1 animated:NO];
        } else {
            [self.textBackgroundPicker setTitle:@"Text" forSegmentAtIndex:0];
            [self.textBackgroundPicker setTitle:@"Background" forSegmentAtIndex:1];
        }
    }
    else if(text) {
        [self.textBackgroundPicker removeSegmentAtIndex:1 animated:NO];
        [self.textBackgroundPicker setTitle:@"Text" forSegmentAtIndex:0];
    } else {
        [self.textBackgroundPicker removeSegmentAtIndex:1 animated:NO];
        [self.textBackgroundPicker setTitle:@"Background" forSegmentAtIndex:0];
    }
    [self.textBackgroundPicker setSelectedSegmentIndex:0];
}

-(void)close {
    [self.wheel close];
}

@end
