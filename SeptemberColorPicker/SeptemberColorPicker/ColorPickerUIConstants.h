//
//  ColorPickerUIConstants.h
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/16/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ColorPickerUIConstants.h"

#ifndef ColorPickerUIConstants

#pragma mark - Sizing
extern const CGFloat CPVerticalSize;
extern const CGFloat CPHorizontalSize;

#pragma mark - Close Button Center
extern const CGFloat CPCloseButtonXCenter;
extern const CGFloat CPCloseButtonYCenter;

#pragma mark - Segment Control Constants
extern const CGFloat CPSegmentXOrigin;
extern const CGFloat CPSegmentYOrigin;
extern const CGFloat CPSegmentWidth;
extern const CGFloat CPSegmentHeight;

#pragma mark - Card sizes
extern const CGFloat CPAlphaControlXOrigin;
extern const CGFloat CPAlphaControlYOrigin;

#pragma mark - Brightness Control Center
extern const CGFloat CPBrightnessControlXOrigin;
extern const CGFloat CPBrightnessControlYOrigin;

#pragma mark - Color Display Constants
extern const CGFloat CPDisplayXOrigin;
extern const CGFloat CPDisplayYOrigin;
extern const CGFloat CPDisplayWidth;
extern const CGFloat CPDisplayHeight;

#pragma mark - Color Wheel Constants
extern const CGFloat CPWheelXOrigin;
extern const CGFloat CPWheelYOrigin;

#pragma mark - Text Field Constants
extern const CGFloat TextFieldHeight;
extern const CGFloat TextFieldWidth;
extern const CGFloat TextFieldOriginX;
extern const CGFloat TextFieldOriginY;
extern const CGFloat TextFieldSpacing;

#endif
