//
//  ColorPickerUIConstants.m
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/16/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import "ColorPickerUIConstants.h"

/** Color Picker Default Size */
const CGFloat CPVerticalSize = 300.0f;
const CGFloat CPHorizontalSize = 200.0f;

/** Color Picker Button Offsets */
const CGFloat CPCloseButtonXCenter = 17.0f;
const CGFloat CPCloseButtonYCenter = 18.0f;

/** Color Picker Segment Constants */
const CGFloat CPSegmentXOrigin = 34.0f;
const CGFloat CPSegmentYOrigin = 5.0f;
const CGFloat CPSegmentWidth = 162.0f;
const CGFloat CPSegmentHeight = 25.0f;

/** Color Picker Alpha Control */
const CGFloat CPAlphaControlXOrigin = 5.0f;
const CGFloat CPAlphaControlYOrigin = 200.0f;

/** Color Picker Brightness Control Center */
const CGFloat CPBrightnessControlXOrigin = 162.0f;
const CGFloat CPBrightnessControlYOrigin = 33.0f;

/** Color Picker Display Constants */
const CGFloat CPDisplayXOrigin = 4.0f;
const CGFloat CPDisplayYOrigin = 38.0f;
const CGFloat CPDisplayWidth = 158.0f;
const CGFloat CPDisplayHeight = 20.0f;

/** Color Picker Wheel Center */
const CGFloat CPWheelXOrigin = 8.0f;
const CGFloat CPWheelYOrigin = 64.0f;

/** Text Fields */
const CGFloat TextFieldHeight = 30.0f;
const CGFloat TextFieldWidth = 45.0f;
const CGFloat TextFieldOriginX = 12.f;
const CGFloat TextFieldOriginY = 250.0f;
const CGFloat TextFieldSpacing = 20.f;
