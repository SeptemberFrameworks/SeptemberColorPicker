//
//  ColorWheelControlConstants.h
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/16/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef ColorWheelControlConstants

#pragma mark - ColorWheel Size Constants
extern const CGFloat ColorWheelControlWidth;
extern const CGFloat ColorWheelControlHeight;

#pragma mark - ColorWheel Selector Constants
extern const CGFloat ColorWheelSelectorWidth;
extern const CGFloat ColorWheelSelectorHeight;
extern const int ColorWheelSelectorBorderWidth;

#endif
