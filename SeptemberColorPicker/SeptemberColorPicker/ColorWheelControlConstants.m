//
//  ColorWheelControlConstants.m
//  SeptemberColorPicker
//
//  Created by Rohan Thomare on 1/16/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import "ColorWheelControlConstants.h"

#ifndef ColorWheelControlConstants

#pragma mark - ColorWheel Size Constants
const CGFloat ColorWheelControlWidth = 150.0f;
const CGFloat ColorWheelControlHeight = 150.0f;

#pragma mark - ColorWheel Selector Constants
const CGFloat ColorWheelSelectorWidth = 6.0f;
const CGFloat ColorWheelSelectorHeight = 6.0f;
const int ColorWheelSelectorBorderWidth = 1.0f;

#endif
