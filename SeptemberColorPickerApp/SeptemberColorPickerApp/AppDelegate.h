//
//  AppDelegate.h
//  SeptemberColorPickerApp
//
//  Created by Rohan Thomare on 1/7/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

