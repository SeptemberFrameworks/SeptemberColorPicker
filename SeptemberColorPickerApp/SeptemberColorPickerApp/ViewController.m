//
//  ViewController.m
//  SeptemberColorPickerApp
//
//  Created by Rohan Thomare on 1/7/14.
//  Copyright © 2014 Rohan Thomare. All rights reserved.
//

#import "ViewController.h"
#import <SeptemberColorPicker/ColorPicker.h>

@interface ViewController () <ColorPickerDelegate>

@property (nonatomic, strong) ColorPicker* cp;
@property (nonatomic, strong) UIView *frontView;
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // load display elements
    _frontView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
    _frontView.backgroundColor = [UIColor whiteColor];
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 40.0f, self.view.frame.size.width, 50.0f)];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.text = @"Where is the color?!?!?!";
    [self.view addSubview:_frontView];
    [self.view addSubview:_titleLabel];

    // load colorpicker
    self.cp = [ColorPicker colorPickerWithDelegate:self];
    self.cp.center = self.view.center;
    [self.view addSubview:self.cp];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ColorPicker:(ColorPicker*)picker createdNewColor:(UIColor*)color withSegmentControlValue:(NSInteger)selection{
    if (selection == 0) {
        // Text update
        [_titleLabel setTextColor:color];
        
    } else {
        // Background Update
        [_frontView setBackgroundColor:color];
    }
}

-(void)ColorPickerPressedExitButton:(ColorPicker*)picker {
    NSLog(@"Exit Pressed");
}

-(BOOL)ColorPickerShouldHaveTextOption:(ColorPicker*)picker {
    return YES;
}

-(BOOL)ColorPickerShouldHaveBackgroundOption:(ColorPicker *)picker {
    return YES;
}

-(BOOL)ColorPickerShouldHaveAlphaControl:(ColorPicker *)picker {
    return YES;
}

@end
